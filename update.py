#!/usr/bin/env python3

import requests
from datetime import datetime, timezone, timedelta
from feedgen.feed import FeedGenerator
from html import unescape

def make_podcast(entries):
  fg = FeedGenerator()
  fg.load_extension('podcast')
  
  fg.id('http://radiodeseo.com/programa-trajines/')
  fg.title('Trajines')
  fg.author( {'name': 'Radio Deseo'} )
  fg.description('Trajinar es llevar y acarrear mercaderías de un lugar a otro, andar y moverse constantemente.  El término trajín y trajinantes aparece frecuentemente en la documentación del período colonial.  Luis Miguel Glave tituló su libro de 1989 como Trajinantes. En él describió el sistema complejo de comercio a gran distancia durante el siglo XVI y XVII a través del sistema de caminos y tambos realizado por los indígenas en sus llamas, llevando lo que se conocía por entonces como “productos de la tierra”.  Pero hay que señalar también que Trajín se asocia también a las mujeres.  Son ellas las que estaban vendiendo y venden aún, en mercados y plazas.  Es por ello que este espacio radial les ofrecemos movimientos de libros, de ideas, de trabajos y de historias.  En Trajines escuchamos y preguntamos con conocimiento de causa: es lo que merecen las personas investigadoras que entrevistamos y es lo que merece la audiencia de Radio Deseo.')
  fg.link( href='http://radiodeseo.com/programa-trajines/', rel='alternate' )
  fg.logo('portada.png')
  fg.podcast.itunes_image('https://mauforonda.gitlab.io/trajines/portada.png')

  for entry in reversed(entries):
    fe = fg.add_entry()
    fe.id(entry['audio'])
    fe.title(entry['title'])
    fe.description(entry['description'])
    fe.enclosure(entry['audio'], 0, entry['mime'])
    fe.published(entry['date'])

  fg.rss_str(pretty=True)
  fg.rss_file('trajines.xml')

def get_data():
  response = requests.get('http://radiodeseo.com/wp-json/wp/v2/media?media_type=audio&search=TRAJINES&per_page=30').json()
  entries = []
  for item in response:
    entry = {}
    entry['title'] = unescape(item['title']['rendered'])
    entry['audio'] = item['source_url']
    entry['mime'] = item['mime_type']
    entry['description'] = item['description']['rendered']
    entry['date'] = datetime.strptime(item['date'], '%Y-%m-%dT%H:%M:%S').astimezone(timezone(timedelta(hours=-4)))
    entries.append(entry)
  return entries

entries = get_data()
make_podcast(entries)

